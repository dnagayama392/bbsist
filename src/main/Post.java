/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 * 投稿一単位の情報を保持するクラス
 * @author nagayama
 */
public class Post{
    //<editor-fold defaultstate="collapsed" desc="const">
    private static final int    DEFAULT_ID      = -1;
    private static final String DEFAULT_NAME    = CommonsText.EMPTY;
    private static final String DEFAULT_CONTENT = CommonsText.EMPTY;
    private static final String DEFAULT_DATE    = CommonsText.EMPTY;
    private static final String DEFAULT_IMAGE   = CommonsText.EMPTY;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="member variable">
    private int id;
    private String name;
    private String content;
    private String date;
    private String image;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getter">
    public int getID(){
        return id;
    };
    public String getName(){
        return name;
    };
    public String getContent(){
        return content;
    };
    public String getDate(){
        return date;
    };
    public String getImage(){
        return image;
    };
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setter">
    public void setID(int id) {
        if(this.id != id) {
            this.id = id;
        }
    }
    public void setName(String name) {
        String newName = selectNewStr(DEFAULT_NAME, name);
        if(substitutionCheck(this.name, newName)) {
            this.name = newName;
        }
    }
    public void setContent(String content) {
        String newContent = selectNewStr(DEFAULT_CONTENT, content);
        if(substitutionCheck(this.content, newContent)) {
            this.content = newContent;
        }
    }
    public void setDate(String date) {
        String newDate = selectNewStr(DEFAULT_DATE, date);
        if(substitutionCheck(this.date, newDate)) {
            this.date = newDate;
        }
    }
    public void setImage(String image) {
        String newImage = selectNewStr(DEFAULT_IMAGE, image);
        if(substitutionCheck(this.image, newImage)) {
            this.image = newImage;
        }
    }
    //</editor-fold>
    
    public Post(int id, String date, String name, String content, String image) {
        setID(id);
        setName(name);
        setContent(content);
        setDate(date);
        setImage(image);
    }
    
    /**
     * 入力値がnullまたは空ならばデフォルト値を返す。
     * @param DefaultStr
     * @param fromStr
     * @return 
     */
    private String selectNewStr(String DefaultStr, String fromStr) {
        return CommonsIO.selectNewStr(DefaultStr, fromStr);
    }
    
    /**
     * 代入すべきかどうか判定（値が等しければfalse）。
     * @param toStr
     * @param fromStr
     * @return 
     */
    private Boolean substitutionCheck(String toStr, String fromStr)
    {
        return CommonsIO.assigningCheck(toStr, fromStr);
    }
}
