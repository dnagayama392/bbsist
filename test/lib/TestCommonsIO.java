/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import main.CommonsWebIO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nagayama
 */
public class TestCommonsIO {
    
    @Test
    public void TestCreateURL()
    {
        assertNotNull("null値ですん。", CommonsWebIO.createURL("https://www.google.co.jp/"));
        assertNull("非null値ですん。", CommonsWebIO.createURL(""));
    }
    
    public TestCommonsIO() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}