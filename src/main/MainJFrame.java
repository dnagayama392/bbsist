/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.Color;
import java.awt.MediaTracker;
import java.io.File;
import java.nio.file.Path;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author nagayama
 */
public class MainJFrame extends javax.swing.JFrame {

    private MainModel mainModel;
    public DefaultTableModel tableModel;
    private String postedFilePath;
    
    //<editor-fold defaultstate="collapsed" desc=" Getter / Setter ">
    //<editor-fold defaultstate="collapsed" desc=" Getter ">
    public MainModel getMainModel()
    {
        return mainModel;
    }
    
    public DefaultTableModel getTableModel()
    {
        return tableModel;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Setter ">
    public void setMainModel(MainModel mainModel)
    {
        this.mainModel = mainModel;
    }

    public void setTableModel(DefaultTableModel defaultTableModel)
    {
        tableModel = defaultTableModel;
    }
    //</editor-fold>
    //</editor-fold>
    
    /**
     * Creates new form MainJFrame
     */
    public MainJFrame() {
        initComponents();
        mainModel = new MainModel();
        // セルエディタを設定しない（編集不可にする）
        jTable1.setDefaultEditor(Object.class, null);
        // 初期設定
        mainModel.setBBSNum(MainModel.BBS_NUM_NAGA);
        showInJTable();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jButtonKOI = new javax.swing.JButton();
        jButtonNAGA = new javax.swing.JButton();
        jPanelPostList = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanelPostView = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jPanelSubmit = new javax.swing.JPanel();
        jLabelPostName = new javax.swing.JLabel();
        jTextFieldPostName = new javax.swing.JTextField();
        jLabelPostEMail = new javax.swing.JLabel();
        jTextFieldPostEMail = new javax.swing.JTextField();
        jLabelPostContnt = new javax.swing.JLabel();
        jScrollPanePostContent = new javax.swing.JScrollPane();
        jTextAreaPostContent = new javax.swing.JTextArea();
        jLabelPostFile = new javax.swing.JLabel();
        jButtonSelectFile = new javax.swing.JButton();
        jLabelPostFileName = new javax.swing.JLabel();
        jButtonSubmit = new javax.swing.JButton();
        jLabelFileImage = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("メイリオ", 1, 18)); // NOI18N
        jLabel1.setText("BBS");

        jButtonKOI.setText("KOI");
        jButtonKOI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonKOIActionPerformed(evt);
            }
        });

        jButtonNAGA.setText("NAGA");
        jButtonNAGA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNAGAActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(jTable1);

        javax.swing.GroupLayout jPanelPostListLayout = new javax.swing.GroupLayout(jPanelPostList);
        jPanelPostList.setLayout(jPanelPostListLayout);
        jPanelPostListLayout.setHorizontalGroup(
            jPanelPostListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPostListLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 543, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelPostListLayout.setVerticalGroup(
            jPanelPostListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPostListLayout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTextArea2.setColumns(20);
        jScrollPane3.setViewportView(jTextArea2);

        javax.swing.GroupLayout jPanelPostViewLayout = new javax.swing.GroupLayout(jPanelPostView);
        jPanelPostView.setLayout(jPanelPostViewLayout);
        jPanelPostViewLayout.setHorizontalGroup(
            jPanelPostViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPostViewLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanelPostViewLayout.setVerticalGroup(
            jPanelPostViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPostViewLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabelPostName.setText("名前");

        jLabelPostEMail.setText("E-Mail");

        jLabelPostContnt.setText("内容");

        jTextAreaPostContent.setColumns(20);
        jTextAreaPostContent.setRows(5);
        jScrollPanePostContent.setViewportView(jTextAreaPostContent);

        jLabelPostFile.setText("添付File");

        jButtonSelectFile.setText("ファイルを選択");
        jButtonSelectFile.setMargin(new java.awt.Insets(2, 5, 2, 5));
        jButtonSelectFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSelectFileActionPerformed(evt);
            }
        });

        jLabelPostFileName.setText("未選択");
        jLabelPostFileName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabelPostFileNameMouseClicked(evt);
            }
        });

        jButtonSubmit.setText("投稿");
        jButtonSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSubmitActionPerformed(evt);
            }
        });

        jLabelFileImage.setPreferredSize(new java.awt.Dimension(100, 100));

        javax.swing.GroupLayout jPanelSubmitLayout = new javax.swing.GroupLayout(jPanelSubmit);
        jPanelSubmit.setLayout(jPanelSubmitLayout);
        jPanelSubmitLayout.setHorizontalGroup(
            jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSubmitLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelSubmitLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonSubmit))
                    .addGroup(jPanelSubmitLayout.createSequentialGroup()
                        .addGroup(jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabelPostEMail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPostContnt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPostFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabelPostName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldPostEMail, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPanePostContent, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanelSubmitLayout.createSequentialGroup()
                                .addComponent(jButtonSelectFile)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelPostFileName, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabelFileImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextFieldPostName))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanelSubmitLayout.setVerticalGroup(
            jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSubmitLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldPostName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPostName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPostEMail)
                    .addComponent(jTextFieldPostEMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPanePostContent, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPostContnt))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanelSubmitLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelPostFile)
                    .addComponent(jButtonSelectFile)
                    .addComponent(jLabelPostFileName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabelFileImage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonSubmit)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonKOI, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonNAGA)
                        .addGap(17, 17, 17))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanelPostList, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanelPostView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonKOI)
                    .addComponent(jButtonNAGA)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanelPostList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanelPostView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanelSubmit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonKOIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonKOIActionPerformed
        mainModel.setBBSNum(MainModel.BBS_NUM_KOIK);
        showInJTable();
    }//GEN-LAST:event_jButtonKOIActionPerformed

    private void jButtonNAGAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNAGAActionPerformed
        mainModel.setBBSNum(MainModel.BBS_NUM_NAGA);
        showInJTable();
    }//GEN-LAST:event_jButtonNAGAActionPerformed

    private void jButtonSelectFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSelectFileActionPerformed
        openFileChooser();
    }//GEN-LAST:event_jButtonSelectFileActionPerformed

    private void jLabelPostFileNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabelPostFileNameMouseClicked
        openFileChooser();
    }//GEN-LAST:event_jLabelPostFileNameMouseClicked

    private void jButtonSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSubmitActionPerformed
        // TODO add your handling code here:
        if(jTextAreaPostContent.getText().equals("")) {
            return;
        }
        mainModel.submitPost(jTextFieldPostName.getText(), jTextFieldPostEMail.getText(), jTextAreaPostContent.getText(), postedFilePath);
    }//GEN-LAST:event_jButtonSubmitActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //<editor-fold desc=" Look and feel setting code (win like) ">
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }
    
    private void showInJTable() {
        // 新しいモデルを取得
        setTableModel(mainModel.getTableModel());
        // jTableとモデルを関連付ける
        jTable1.setModel(getTableModel());
        jTable1.setAutoCreateRowSorter(true);
        jTable1.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override public void valueChanged(ListSelectionEvent e) {
                if(e.getValueIsAdjusting()) return;
                int sc = jTable1.getSelectedRowCount();
                showPost((sc==1) ? getInfo() : " ");
            }
        });
        // jTableの列幅の設定
        DefaultTableColumnModel columnModel = (DefaultTableColumnModel)jTable1.getColumnModel();
        TableColumn column = null;
        for(int i = 0; i < columnModel.getColumnCount(); i++) {
            column = columnModel.getColumn(i);
            column.setPreferredWidth(50 + i * 20);
        }
        columnModel.getColumn(0).setPreferredWidth(30);
        columnModel.getColumn(2).setPreferredWidth(175);
        columnModel.getColumn(4).setPreferredWidth(35);
    }
    
    private String getInfo() {
        DefaultTableModel model = getTableModel();
        int index = jTable1.convertRowIndexToModel(jTable1.getSelectedRow());
        String name = (String)model.getValueAt(index, 1);
        String content = (String)model.getValueAt(index, 2);
        return name + ": \r\n" + content + "";
    }
    
    private void openFileChooser() {
//        jLabelFileImage = new JLabel(); // TODO: 再選択->取消　のために空にする必要あり
        jLabelPostFileName.setText("未選択");
        JFileChooser fc = new JFileChooser();
        // 画像ファイルの拡張子を設定
        fc.setFileFilter(new FileNameExtensionFilter(
                "画像ファイル (*.png, *.jpg, *.jpeg, *.gif)", "png", "jpg", "jpeg", "gif"));
        // ファイル選択ダイアログを表示、戻り値がAPPROVE_OPTIONの場合、画像ファイルを開く
        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File f = fc.getSelectedFile();
            jLabelPostFileName.setText(f.getName());
            // アイコンを取得
            postedFilePath = f.getPath();
            ImageIcon icon = new ImageIcon(postedFilePath);
            MediaTracker tracker = new MediaTracker(this);
            icon = mainModel.getPostedImageIcon(icon, tracker);
            // アイコンをラベルに設定
            jLabelFileImage.setIcon(icon);
        }
    }
    
    private void showPost(String str) {
        jTextArea2.setText(str);
        jTextArea2.setEnabled(false);
        jTextArea2.setDisabledTextColor(Color.BLACK);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonKOI;
    private javax.swing.JButton jButtonNAGA;
    private javax.swing.JButton jButtonSelectFile;
    private javax.swing.JButton jButtonSubmit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabelFileImage;
    private javax.swing.JLabel jLabelPostContnt;
    private javax.swing.JLabel jLabelPostEMail;
    private javax.swing.JLabel jLabelPostFile;
    private javax.swing.JLabel jLabelPostFileName;
    private javax.swing.JLabel jLabelPostName;
    private javax.swing.JPanel jPanelPostList;
    private javax.swing.JPanel jPanelPostView;
    private javax.swing.JPanel jPanelSubmit;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPanePostContent;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextAreaPostContent;
    private javax.swing.JTextField jTextFieldPostEMail;
    private javax.swing.JTextField jTextFieldPostName;
    // End of variables declaration//GEN-END:variables
}
