/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.Image;
import java.awt.MediaTracker;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author nagayama
 */
public class MainModel
{
    public static final int BBS_NUM_KOIK = CommonsBBS.BBS_NUM_KOIK;
    public static final int BBS_NUM_NAGA = CommonsBBS.BBS_NUM_NAGA;
    public static final String[] COLUMN_NAMES = {"No.", "Name", "Content", "Date", "Image"};
    public static final double MAX_LENGTH_EACH_IMAGE_SIDE = 100.0;
    
    private APIReader apiReader;
    private int bbsNum;
    private ArrayList<PostDataPair> postData;
    
    // <editor-fold defaultstate="collapsed" desc="Getter Setter">
    public APIReader getAPIReader()
    {
        return apiReader;
    }
    
    public int getBBSNum()
    {
        return bbsNum;
    }
    
    public void setAPIReader(APIReader apiReader)
    {
        this.apiReader = apiReader;
    }
    
    public void setBBSNum(int bbsNum)
    {
        if(this.bbsNum != bbsNum)
        {
            this.bbsNum = bbsNum;
        }
    }
    
    //</editor-fold>
    
    public MainModel()
    {
        setAPIReader(new APIReader());
        setBBSNum(BBS_NUM_NAGA);
    }
    
    /**
     * 投稿画像のサムネイルを作成して返す。
     * @param icon
     * @param tracker
     * @return 
     */
    public ImageIcon getPostedImageIcon(ImageIcon icon, MediaTracker tracker)
    {
        double resizeScale = calcResizeScale(icon);
        // 縮小率が1.0（縮小なし）ならば、そのまま返す。
        if(resizeScale == 1.0)
        {
            return icon;
        }
        // 画像の大きさを変更
        Image resizeImg = icon.getImage().getScaledInstance((int) (icon.getIconWidth() * resizeScale), -1,
                Image.SCALE_SMOOTH);
        tracker.addImage(resizeImg, 1);
        ImageIcon resizeIcon = new ImageIcon(resizeImg);
        return resizeIcon;
    }
    
    /**
     * BBSのテーブルモデルを取得
     * @return 
     */
    public DefaultTableModel getTableModel() {
        // 新しいテーブルモデルの作成
        DefaultTableModel tblModel;
        tblModel = new DefaultTableModel(COLUMN_NAMES, 0);
        // あとは要素を流すだけ
        ArrayList<String[]> postArys = getPostArrays();
        for(String[] postAry : postArys) {
            tblModel.addRow(postAry);
        }
        return tblModel;
    }
    
    public void submitPost(String name, String eMail, String content, String imagePath)
    {
        postData = new ArrayList<>();
        postData.add(new PostDataPair(getNameOfName(), name));
        if(getBBSNum() == BBS_NUM_NAGA) {
            postData.add(new PostDataPair("email", eMail));
        }
        if(getBBSNum() == BBS_NUM_KOIK) {
            postData.add(new PostDataPair("title", ""));
        }
        postData.add(new PostDataPair(getNameOfContent(), content));
        PostSubmitter ps = new PostSubmitter(getBBSNum(), postData, imagePath);
        ps.submitMIME();
    }
    
    //<editor-fold defaultstate="collapsed" desc="private methods">
    /**
     * 既定サイズへ縮小するための縮小率を返す。
     * @param icon
     * @return double 縮小率（≦1.0）
     */
    private double calcResizeScale(ImageIcon icon) {
        double scale = 1.0;
        double height = icon.getIconHeight();
        double width = icon.getIconWidth();
        double refValue = Math.max(height, width);
        if(refValue < MAX_LENGTH_EACH_IMAGE_SIDE)
        {
            return scale;
        }
        return MAX_LENGTH_EACH_IMAGE_SIDE / refValue;
    }
    
    private ArrayList<String[]> getPostArrays() {
        ArrayList<String[]> postArys = new ArrayList<>();
        ArrayList<Post> postList;
        postList = getAPIReader().getContentList(getBBSNum());
        if(postList.isEmpty())
        {
            String[] errormessage = {"投稿情報が正常に取得できませんでした。"};
            postArys.add(errormessage);
            return postArys;
        }
        postArys.addAll(toPostArys(postList));
        return postArys;
    }
    /**
     * 投稿者名の name を返す。
     * @return 
     */
    private String getNameOfName()
    {
        return CommonsBBS.getNameOfName(getBBSNum());
    }
    
    /**
     * 投稿内容の name を返す。
     * @return 
     */
    private String getNameOfContent()
    {
        return CommonsBBS.getNameOfContent(getBBSNum());
    }
    
    /**
     * リストを一行の文字列表示に
     * @param postList
     * @return 
     */
    private ArrayList<String[]> toPostArys(ArrayList<Post> postList) {
        ArrayList<String[]> postArys = new ArrayList<>();
        int i = postList.size();
        for (Post post :postList){
            String[] postAry = {Integer.toString(i), post.getName(), post.getContent(), post.getDate(), post.getImage()};
            postArys.add(postAry);
            i--;
        }
        return postArys;
    }
    //</editor-fold>
}
