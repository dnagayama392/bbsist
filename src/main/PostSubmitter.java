/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nagayama
 */
public class PostSubmitter {
    
    private static final String BOUNDARY = "----------V2ymHFg03ehbqgZCaKO6jy";
    private static final String LFCR = CommonsText.LFCR;
    private static final String DEFAULT_IMAGE_PATH = CommonsText.EMPTY;
    private static final String DEFAULT_IMAGE_NAME = CommonsText.EMPTY;
   
    private List<PostDataPair> postData;
    private String imageName;
    private String imagePath;
    private String entryURL;
    private String referer;
    
    //<editor-fold defaultstate="collapsed" desc="getter">
     private String getImageName() {
         return imageName;
     }
     
     private String getImagePath() {
         return imagePath;
     }
     
     private String getEntryURL() {
         return entryURL;
     }
     
     private String getReferer() {
         return referer;
     }
     
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setter">
     private void setImageName(String imageName) {
         String newImageName = CommonsIO.selectNewStr(DEFAULT_IMAGE_NAME, imageName);
         if(CommonsIO.assigningCheck(this.imageName, newImageName)) {
             this.imageName = newImageName;
         }
     }

     private void setImagePath(String imagePath) {
         String newImagePath = CommonsIO.selectNewStr(DEFAULT_IMAGE_PATH, imagePath);
         if(CommonsIO.assigningCheck(this.imagePath, newImagePath)) {
             this.imagePath = newImagePath;
         }
     }
     
     private void setEntryURL(String entryURL) {
         if(CommonsIO.assigningCheck(this.entryURL, entryURL)) {
            this.entryURL = entryURL;
         }
     }
     
     private void setReferer(String referer) {
         if(CommonsIO.assigningCheck(this.referer, referer)) {
            this.referer = referer;
         }
     }
    //</editor-fold>
    
    public PostSubmitter(int bbsNum, ArrayList<PostDataPair> postData, String imagePath) {
        setEntryURL(selectEntryURL(bbsNum));
        this.postData = postData;
        setImageName(selectImageName(bbsNum));
        setImagePath(imagePath);
        setReferer(selectReferer(bbsNum));
    }
    
    public void submitMIME() {
        // 接続オブジェクト
        URLConnection conn = null;
        // 投稿認証用の値
        String tokenID = "DTYghf81HIUIyhoi8843j1";
        // 接続開始
        try {
            conn = (CommonsWebIO.createURL(getEntryURL())).openConnection();
            conn.setDoOutput(true);
            // Entryはreferer制限がかかっているため、投稿可能なrefererをヘッダーにセットする。
            conn.setRequestProperty("REFERER", getReferer());
            // cookieに認証値を渡す
            conn.setRequestProperty("Cookie", "token=" + tokenID);
            // 送信データ１行目に書き込まれる。
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
            
            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            // sent token
            writeTextField(out, "token", tokenID);
            // sent text
            for (PostDataPair nv : postData) {
                writeTextField(out, nv.getName(), nv.getValue());
            }
            // sent image
            writeFileField(out);
            // 送信終わり
            out.writeBytes("--" + BOUNDARY + "--");
            out.flush();
            out.close();
            
            // レスポンスを受信 (これをやらないと通信が完了しない)
            InputStream stream = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String responseData = null;
            while((responseData = reader.readLine()) != null) {
                System.out.print(responseData);
            }
            stream.close();
        } catch (IOException ex) {
            Logger.getLogger(PostSubmitter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(conn != null) {
                ((HttpURLConnection)conn).disconnect();
            }
        }
    }

    private String selectImageName(int bbsNum) {
        return CommonsBBS.getNameOfFile(bbsNum);
    }
    
    private String selectEntryURL(int bbsNum) {
        return CommonsBBS.getEntryURL(bbsNum);
    }
    
    private String selectReferer(int bbsNum) {
        return CommonsBBS.getReferer(bbsNum);
    }
    
    private void writeTextField(DataOutputStream out, String textName, String textValue) {
        try {
            out.writeBytes("--" + BOUNDARY + LFCR);
            out.writeBytes("Content-Disposition: form-data; name=\"" + textName + "\""+ LFCR);
            out.writeBytes(LFCR);
            out.write(textValue.getBytes("UTF-8"));
            out.writeBytes(LFCR);
        } catch (IOException ex) {
            Logger.getLogger(PostSubmitter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeFileField(DataOutputStream out) {
        if(!getImagePath().isEmpty()) {
            File file = new File(getImagePath());
            if(!file.getName().equals("未選択")) {
                try {
                    out.writeBytes("--" + BOUNDARY + LFCR);
                    out.writeBytes("Content-Disposition: form-data; name=\"" + getImageName() + "\"; filename=\""); // TODO 要KoiCage対応
                    out.write(file.getName().getBytes("UTF-8"));
                    out.writeBytes("\"" + LFCR);
                    out.writeBytes("Content-Type: application/octet-stream" + LFCR + LFCR);
                    BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
                    int buff = 0;
                    while((buff = in.read()) != -1) {
                        out.write(buff);
                    }
                    out.writeBytes(LFCR);
                } catch (IOException ex) {
                    Logger.getLogger(PostSubmitter.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
