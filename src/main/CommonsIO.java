/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author nagayama
 */
public class CommonsIO {
 
    public CommonsIO() {
        
    }
    
    /**
     * 入力値がnullまたは空ならばデフォルト値を返す。
     * @param DefaultStr
     * @param fromStr
     * @return 
     */
    public static String selectNewStr(String DefaultStr, String fromStr)
    {
        if(fromStr == null)
        {
            return DefaultStr;
        }
        if(fromStr.isEmpty())
        {
            return DefaultStr;
        }
        return fromStr;
    }

    /**
     * 代入すべきかどうか判定（値が等しければfalse）。
     * @param toStr
     * @param fromStr
     * @return 
     */
    public static Boolean assigningCheck(String toStr, String fromStr)
    {
        if(toStr == null)
        {
            return true;
        }
        if(!toStr.equals(fromStr))
        {
            return true;
        }
        return false;
    }
}
