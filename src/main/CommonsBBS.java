/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author nagayama
 */
public class CommonsBBS {
    public static final int BBS_NUM_KOIK = 88;
    public static final int BBS_NUM_NAGA = 89;
    public static final String ENTRY_URL_088 = "http://192.168.5.7:8080/KoiCage/Entry";
    public static final String ENTRY_URL_089 = "http://192.168.5.40:8081/HelloWorld/Entry_from_App";
    private static final String NAME_OF_NAME_088 = "name";
    private static final String NAME_OF_NAME_089 = "username";
    private static final String NAME_OF_CONTENT_088 = "content";
    private static final String NAME_OF_CONTENT_089 = "message";
    private static final String NAME_OF_FILE_088 = "file";
    private static final String NAME_OF_FILE_089 = "datafile";
    public static final String REFERER_088 = "http://192.168.5.7:8080/KoiCage/Hello";
    public static final String REFERER_089 = "http://192.168.5.40:8081/HelloWorld/BBS";
    
    /**
     * 投稿先URLを返す。
     * @param bbsNum
     * @return 
     */
    public static String getEntryURL(int bbsNum) {
        switch(bbsNum) {
            case 88:
                return ENTRY_URL_088;
            case 89:
            default:
                return ENTRY_URL_089;
        }
    }
    
    /**
     * 添付ファイル input の name を返す。
     * @param bbsNum
     * @return 
     */
    public static String getNameOfFile(int bbsNum)
    {
        switch(bbsNum)
        {
            case 88:
                return NAME_OF_FILE_088;
            case 89:
            default:
                return NAME_OF_FILE_089;
        }
    }
    
    /**
     * 投稿者名の name を返す。
     * @param bbsNum
     * @return 
     */
    public static String getNameOfName(int bbsNum)
    {
        switch(bbsNum)
        {
            case 88:
                return NAME_OF_NAME_088;
            case 89:
            default:
                return NAME_OF_NAME_089;
        }
    }
    
    /**
     * 投稿内容の name を返す。
     * @param bbsNum
     * @return 
     */
    public static String getNameOfContent(int bbsNum)
    {
        switch(bbsNum)
        {
            case 88:
                return NAME_OF_CONTENT_088;
            case 89:
            default:
                return NAME_OF_CONTENT_089;
        }
    }
    
    /**
     * BBSの Referer を返す。
     * @param bbsNum
     * @return 
     */
    public static String getReferer(int bbsNum)
    {
        switch(bbsNum)
        {
            case 88:
                return REFERER_088;
            case 89:
            default:
                return REFERER_089;
        }
    }
}
