/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author nagayama
 */
public class APIReader
{
    private static final String API_URL_088 = "http://192.168.5.7:8080/KoiCage/KoiCageAPIappend";
    private static final String API_URL_089 = "http://192.168.5.40:8081/HelloWorld/BBSAPI_for_App";
    private static final String TAG_NAME_POST    = "post";
    private static final String TAG_NAME_ID      = "id";
    private static final String TAG_NAME_NAME    = "name";
    private static final String TAG_NAME_CONTENT = "content";
    private static final String TAG_NAME_DATE    = "date";
    private static final String TAG_NAME_IMAGE   = "image";

    public APIReader()
    {
        
    }
    
    public ArrayList<Post> getContentList(int bbsNum)
    {
        ArrayList<Post> resultList = new ArrayList<>();
        // 接続オブジェクト
        HttpURLConnection http = selectAPIConection(bbsNum);
        try
        {
            // 接続
            http.connect();
            // XML 取得
            // Documentオブジェクトを取得
            Document doc = getXMLDoc(http);
            // ルート要素
            Element root = doc.getDocumentElement(); // 接続できないとき、ここで NullPointerException
            // post (投稿情報) の一覧を取得
            NodeList postList = root.getElementsByTagName(TAG_NAME_POST);
            // 一つ目のノード
            for(int i = 0;i < postList.getLength(); i++)
            {
                Node nPost = postList.item(i);
                Post post = convertPost(nPost);
                resultList.add(post);
            }
        }
        catch (IOException ex) // createGETURLConection に失敗しました。
        {
            Logger.getLogger(APIReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (NullPointerException ex) //Documentオブジェクトの取得失敗
        {
            Logger.getLogger(APIReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            // 接続解除
            http.disconnect();
        }
        return resultList;
    }
    
    public Post convertPost(Node nPost)
    {
        Node nID = getAloneNode(nPost, TAG_NAME_ID);
        int id = Integer.valueOf(getTextValue(nID));
        Node nDate = getAloneNode(nPost, TAG_NAME_DATE);
        String date = getTextValue(nDate);
        Node nName = getAloneNode(nPost, TAG_NAME_NAME);
        String name = getTextValue(nName);
        Node nContent = getAloneNode(nPost, TAG_NAME_CONTENT);
        String content = getTextValue(nContent);
        Node nImage = getAloneNode(nPost, TAG_NAME_IMAGE);
        String image = getTextValue(nImage);
        return new Post(id, date, name, content, image);
    }
        
    /**
     * 対象ノードの子要素から指定の名前の最初の子ノードを返す。
     * @param node 対象ノード
     * @param childName 子ノード名
     * @return 
     */
    private Node getAloneNode(Node node, String childName)
    {
        Node childNode = null;
        if(childName == null)
        {
            return null;
        }
        NodeList nodeList = ((Element)node).getElementsByTagName(childName);
        if(nodeList.getLength() > 0)
        {
            childNode = nodeList.item(0);
        }
        return childNode;
    }
    
    /**
     * タグのテキストノードを返す。
     * @param node 対象タグのノード
     * @return String, 子要素にテキストノードが無ければ null
     */
    private String getTextValue(Node node)
    {
        String text = null;
        if(node == null)
        {
            return null;
        }
        Node txNode = node.getFirstChild();
        if(txNode == null)
        {
            return null;
        }
        if(txNode.getNodeType() == Node.TEXT_NODE)
        {
            text = txNode.getNodeValue();
        }
        return text;
    }
    
    public Document getXMLDoc(HttpURLConnection http)
    {
        Document doc = null;
        try
        {
            // XML 取得の準備
            DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbfactory.newDocumentBuilder();
            // InputStream から Documentオブジェクトを取得
            doc = builder.parse(http.getInputStream());
        }
        catch (ParserConfigurationException ex)
        {
            Logger.getLogger(APIReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SAXException ex)
        {
            Logger.getLogger(APIReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            return doc;
        }
    }
    
    private HttpURLConnection selectAPIConection(int number)
    {
        switch(number)
        {
            case 88:
                return CommonsWebIO.createGETURLConection(API_URL_088);
            case 89:
            default:
                return CommonsWebIO.createGETURLConection(API_URL_089);
        }
    }
}
