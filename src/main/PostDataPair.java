/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 * org.apache.http.NameValuePair のような、
 * 文字列の組み合わせのクラスが見つけられなかったので
 * @author nagayama
 */
public class PostDataPair {
    private String name;
    private String value;
    
    //<editor-fold defaultstate="collapsed" desc="getter">
    public String getName()
    {
        return name;
    }
    
    public String getValue() {
        return value;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setter">
    private void setName(String name) {
        if(CommonsIO.assigningCheck(this.name, name)) {
            this.name = name;
        }
    }
    
    private void setValue(String value) {
        if(CommonsIO.assigningCheck(this.value, value)) {
            this.value = value;
        }
    }
    //</editor-fold>
    
    public PostDataPair(String name, String value) {
        setName(name);
        setValue(value);
    }
}
