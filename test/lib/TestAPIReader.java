/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lib;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.APIReader;
import main.CommonsWebIO;
import main.Post;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nagayama
 */
public class TestAPIReader {
        
    private static final String API_URL = "http://192.168.5.40:8081/HelloWorld/BBSAPI_for_App";
    @Test
    public void getContentListTest()
    {
        Post expected = new Post(1,"2003/08/05(火) 09:38:16","ごまだれ","ヽ( ･ω･)ﾉ","");
        ArrayList<Post> getList = new APIReader().getContentList(89);
        Post actual = getList.get(getList.size() - 1);
        assertEquals(expected.getID(), actual.getID());
        assertEquals(expected.getDate(), actual.getDate());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getContent(), actual.getContent());
        assertEquals(expected.getImage(), actual.getImage());
    }
    
    @Test
    public void getXMLDocTest()
    {
        HttpURLConnection http = CommonsWebIO.createGETURLConection(API_URL);
        try
        {
            // 接続
            http.connect();
        }
        catch (IOException ex)
        {
            Logger.getLogger(TestAPIReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertNotNull("非Nullにしましょう。", new APIReader().getXMLDoc(http));
        // 接続解除
        http.disconnect();
    }

    @Test
    public void createGETURLConectionTest()
    {
        assertNotNull("非Nullにしましょう。", CommonsWebIO.createGETURLConection(API_URL));
    }
    
    public TestAPIReader() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}