/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nagayama
 */
public class CommonsWebIO {
    private static final String METHOD_GET  = "GET";
    private static final String METHOD_POST  = "POST";

    public static HttpURLConnection createGETURLConection(String urlPath)
    {
        HttpURLConnection http = null;
        try
        {
            // ターゲット
            URL url = CommonsWebIO.createURL(urlPath);
            // 接続オブジェクト
            http = (HttpURLConnection)url.openConnection();
            // GET メソッド
            http.setRequestMethod(METHOD_GET);
        }
        catch (IOException ex) // openConnection に失敗しました。
        {
            Logger.getLogger(APIReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return http;
    }

    public static HttpURLConnection createPOSTURLConection(String urlPath)
    {
        HttpURLConnection http = null;
        try
        {
            // ターゲット
            URL url = CommonsWebIO.createURL(urlPath);
            // 接続オブジェクト
            http = (HttpURLConnection)url.openConnection();
            // POST メソッド
            http.setRequestMethod(METHOD_POST);
            // POSTによるデータ送信を可能にする
            http.setDoOutput(true);
        }
        catch (IOException ex) // openConnection に失敗しました。
        {
            Logger.getLogger(APIReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return http;
    }

    public static URL createURL(String urlPath) {
        URL url = null;
        try {
            // ターゲット
            url = new URL(urlPath);
        }
        catch (MalformedURLException ex) // 不正な形式の URL です
        {
            Logger.getLogger(APIReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return url;
    }
    
}
